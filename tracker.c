/*
 * Program invocation logger using network UDP
 *
 * This program will log the invocation of a program to a remote tracking
 * daemon using UDP.  It's meant to be installed under the name of the program
 * being run and will exec the real program.  A single UDP packet will be
 * sent, and no checks for successful receipt are performed.  This program
 * attempts to be as fast as possible, to do its business and then disappear
 * again.
 *
 * The protocol spoken is the same as the original licensetrack protocol by
 * Larry Schwimmer <opusl@stanford.edu>.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1999, 2009 Board of Trustees, Leland Stanford Jr. University
 *
 * See LICENSE for licensing terms.
 */

#include <config.h>

/* sys/types.h must be before arpa/inet.h on AIX 4.1. */
#include <sys/types.h>

/* Solaris 2.5.1 wants netinet/in.h before arpa/inet.h. */
#include <netinet/in.h>

#include <arpa/inet.h>          /* inet_addr() */
#include <errno.h>              /* errno */
#include <limits.h>             /* PATH_MAX */
#include <netdb.h>              /* gethostbyname() */
#include <netinet/in.h>         /* sockaddr_in, IPPROTO_UDP */
#include <pwd.h>                /* struct passwd, getpwnam() */
#include <stdio.h>              /* stderr, fprintf(), sprintf() */
#include <stdlib.h>             /* exit(), strtol(), strtoul() */
#include <string.h>             /* strlen(), strcpy(), strcat(), etc. */
#include <sys/socket.h>         /* socket(), recvfrom(), AF_INET, etc. */
#include <time.h>               /* time(), difftime() */
#include <unistd.h>             /* getuid() */

#include <tracker.h>            /* Protocol definition */

/* We want to use __attribute__((__unused__)). */
#ifndef __attribute__
# if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 7)
#  define __attribute__(spec)   /* empty */
# endif
#endif

/* We hide the real binaries by giving them a known prefix defined here. */
#define TRACK_PREFIX    ".t%"
#define TRACK_PREFIXLEN 3

/*
 * We prevent looping by putting argv[0], a recursion depth, and a timestamp
 * in this environment variable.  Then each time tracker is invoked, if
 * argv[0] is the same and the current time is within TRACK_TIME seconds of
 * the previous time, increment the counter.  When we reach TRACK_DEPTH,
 * abort.
 */
#define TRACK_ENV       "TRACKING"
#define TRACK_ENVLEN    8
#define TRACK_TIME      1
#define TRACK_DEPTH     10

int
main(int argc __attribute__((__unused__)), char **argv)
{
    static struct track track;
    static char program[PATH_MAX];
    static char env[PATH_MAX + TRACK_ENVLEN + 32];
    static time_t last = 0;
    static int count = 1;
    struct passwd *pwd;
    struct sockaddr_in server;
    struct hostent *hp;
    char *p;
    char save;
    int s;
    size_t len;
    uid_t uid;
    time_t now;

    /* Add PREFIX to the program name in our argv[0] to find what to run. */
    len = strlen(argv[0]);
    if (len + TRACK_PREFIXLEN >= PATH_MAX) {
        fprintf(stderr, "%s: %s\n", argv[0], strerror(ENAMETOOLONG));
        exit(1);
    }
    p = strrchr(argv[0], '/');
    if (p) {
        save = *++p;
        *p = '\0';
        strcpy(program, argv[0]);
        *p = save;
    } else {
        p = argv[0];
    }
    strcat(program, TRACK_PREFIX);
    strcat(program, p);
    strncpy(track.program, p, TRACK_PROGLEN - 1);
    track.program[TRACK_PROGLEN - 1] = '\0';

    /*
     * As above, pull argv[0], the count, and the current time out of our
     * environment variable if it's there.  If the current time is within
     * TRACK_TIME of the timestamp, increment the count, and abort if the
     * count is too high.  Otherwise, put the updated information back into
     * the environment variable.
     */
    p = getenv(TRACK_ENV);
    now = time(NULL);
    if (p && strlen(p) > len && !strncmp(p, argv[0], len) && p[len] == ':') {
        p += len + 1;
        count = strtol(p, &p, 10);
        if (*p == ':')
            last = strtoul(p + 1, NULL, 10);
        if (difftime(now, last) <= TRACK_TIME) {
            if (count++ > TRACK_DEPTH) {
                fprintf(stderr, "%s respawning too rapidly, aborted\n",
                        argv[0]);
                exit(1);
            }
        } else {
            count = 1;
        }
    }
    sprintf(env, "%s=%s:%d:%lu", TRACK_ENV, argv[0], count, now);
    putenv(env);

    /* Determine username if available. */
    uid = getuid();
    pwd = getpwuid(uid);
    if (pwd == NULL)
        sprintf(track.username, "%lu", (unsigned long) uid);
    else {
        strncpy(track.username, pwd->pw_name, TRACK_USERLEN - 1);
        track.username[TRACK_USERLEN - 1] = '\0';
    }

    /* Finish the track packet and send it. */
    track.version = htonl(TRACK_VERSION);
    track.operation = htonl(TRACK_START);
    server.sin_addr.s_addr = inet_addr(TRACK_SERVER);
    if (server.sin_addr.s_addr == (in_addr_t) -1) {
        hp = gethostbyname(TRACK_SERVER);
        if (hp == NULL)
            goto fail;
        memcpy(&server.sin_addr.s_addr, hp->h_addr, hp->h_length);
    }
    server.sin_family = AF_INET;
    server.sin_port = ntohs(TRACK_PORT);
    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s < 0)
        goto fail;
    sendto(s, &track, sizeof(struct track), 0, (struct sockaddr *) &server,
           sizeof(struct sockaddr_in));

    /* exec the real program whether that succeeds or fails. */
 fail:
    if (execvp(program, argv) < 0) {
        fprintf(stderr, "%s: %s\n", argv[0], strerror(errno));
        exit(1);
    }

    /* Not reached. */
    exit(0);
}
