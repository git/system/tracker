/*
 * Program invocation logger protocol definitions.
 *
 * This is version 1 of the licensetrack protocol.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Based on a protocol by Larry Schwimmer <opusl@stanford.edu>
 * Copyright 1999, 2009 Board of Trustees, Leland Stanford Jr. University
 *
 * See LICENSE for licensing terms.
 */

#ifndef TRACKER_H
#define TRACKER_H 1

#include <config.h>

#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#endif
#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#define TRACK_VERSION   1       /* Protocol version. */
#define TRACK_PORT      1738    /* Tracker daemon port number. */

#define TRACK_USERLEN   32      /* Max username length, including nul. */
#define TRACK_PROGLEN   256     /* Max program name length, including nul. */

#define TRACK_START     0       /* Start application */
#define TRACK_STOP      1       /* Exit application (not currently used) */

struct track {
    int32_t version;            /* TRACK_VERSION */
    int32_t operation;          /* TRACK_START or TRACK_STOP */
    char username[TRACK_USERLEN];
    char program[TRACK_PROGLEN];
};

#endif /* TRACKER_H */
