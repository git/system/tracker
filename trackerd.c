/*
 * Program invocation logging daemon.
 *
 * This is the server for the track program invocation tracking system.  It
 * runs as a daemon, listening on the tracker port, and handles incoming UDP
 * packets from tracker clients.  In response, it prints a string of the
 * format:
 *
 *   <username>@<host> <program>
 *
 * to stdout.  <host> is the IP address from which the packet was received;
 * all over values come from the client packet.  There's no authentication.
 *
 * This program does not attempt to manage process groups or do tty
 * disassociation, on the grounds that such actions should be done by
 * specialized programs that know exactly what to do.  It also doesn't use
 * syslog because if you're tracking a large quantity of programs, syslog will
 * be too slow.  It runs as a daemon to avoid inetd and so forth.  Its output
 * should probably be directed to multilog for timestamps and log rotation.
 * Alternately, one could direct the output to splogger to send it to syslog.
 *
 * Written by Russ Allbery <eagle@eyrie.org>
 * Copyright 1999 Board of Trustees, Leland Stanford Jr. University
 */

#include "config.h"

/* sys/types.h must be before arpa/inet.h on AIX 4.1. */
#include <sys/types.h>

/* Solaris 2.5.1 wants netinet/in.h before arpa/inet.h. */
#include <netinet/in.h>

#include <arpa/inet.h>          /* inet_ntoa() */
#include <errno.h>              /* errno */
#include <stdio.h>              /* printf(), fprintf(), stderr, etc. */
#include <stdlib.h>             /* exit() */
#include <string.h>             /* strerror() */
#include <sys/socket.h>         /* socket(), recvfrom(), AF_INET, etc. */
#include <sys/time.h>           /* select(), timeval */

/* AIX needs sys/select.h, but it doesn't exist everywhere. */
#ifdef HAVE_SYS_SELECT_H
# include <sys/select.h>
#endif

#include <tracker.h>            /* Protocol definition */

/* We want to use __attribute__((__unused__)). */
#ifndef __attribute__
# if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 7)
#  define __attribute__(spec)   /* empty */
# endif
#endif

static const char *program = NULL;

static void
die(const char *function)
{
    fprintf(stderr, "%s: %s: %s\n", program, function, strerror(errno));
    exit(1);
}

int
main(int argc __attribute__((__unused__)), char *argv[])
{
    struct track track;
    struct timeval delay;
    struct sockaddr_in sin, from;
    fd_set fds;
    int s, r;
    socklen_t size;

    program = argv[0];

    /* Initialize our socket. */
    sin.sin_family = AF_INET;
    sin.sin_port = htons(TRACK_PORT);
    sin.sin_addr.s_addr = INADDR_ANY;
    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s < 0)
        die("socket");
    if (bind(s, (struct sockaddr *) &sin, sizeof(struct sockaddr_in)) < 0)
        die("bind");

    /* Process packets in an infinite loop. */
    while (1) {
        FD_ZERO (&fds);
        FD_SET (s, &fds);
        r = select(s + 1, &fds, NULL, NULL, NULL);

        /* Assume errors are transient.  Wait for a second and then retry. */
        if (r < 0) {
            fprintf(stderr, "%s: select: %s\n", program, strerror(errno));
            delay.tv_sec = 1;
            delay.tv_usec = 0;
            select(0, NULL, NULL, NULL, &delay);
            continue;
        }

        /* We have a packet; read it in and spit out the output. */
        size = sizeof(struct sockaddr_in);
        r = recvfrom(s, &track, sizeof(struct track), 0,
                     (struct sockaddr *) &from, &size);
        if (r < 0)
            continue;
        if (htonl(track.version) != TRACK_VERSION)
            continue;
        if (htonl(track.operation) != TRACK_START)
            continue;
        track.username[TRACK_USERLEN - 1] = '\0';
        track.program[TRACK_PROGLEN - 1] = '\0';
        printf("%s@%s %s\n", track.username, inet_ntoa(from.sin_addr),
               track.program);
        fflush(stdout);
    }

    /* Never reached. */
    exit (0);
}
